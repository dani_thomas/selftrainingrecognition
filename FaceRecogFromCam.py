#! /usr/bin/env python
#--------------------------------------------------------------------------
# Copyright 2020 Cyber-Renegade.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Name:        FaceRecogFromCam.py
#              
# Purpose:     Detects faces from your web cam or pi cam. edit the 
#              homeFaces.json to correct the labels
#
# Author:      Dani Thomas
#
# Requires:    OpenCV, multiprocenv 
# Based on: 
#-------------------------------------------------------------------------
from miaCamera import ReadCam, DisplayCam
from GoogleImages import GoogleImages
from miaFaceRecog import miaFaceRecog
from miaMultiProcEnv import MiaEnv

readCam=ReadCam(framesize="large")
display=DisplayCam()
faceRecog = miaFaceRecog('homeFaces.pickle','homeFaces.json')
bsPrs=MiaEnv([readCam,faceRecog, display])
