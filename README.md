This repository uses multiprocenv to do some quick and easy face recognition without any pre-training. Requires opencv and dlib.

All that the user is required to do is to edit the labels file in faceData after the first run so the correct name is displayed.