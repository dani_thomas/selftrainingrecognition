#! /usr/bin/env python
#--------------------------------------------------------------------------
# Copyright 2020 Cyber-Renegade.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Name:        FaceRecogCeleb.py
#              
# Purpose:     Detects faces from images on the web. In this case angelina jolie
#              and brad pitt. you can use the angelina.pickle and angelina.json
#              in faceData or create your own. just edit the json to label the
#              faces as required
#
#              eg run python3 FaceRecogCeleb.py -s 'angelina jolie' -e 
#                    'angelina.pickle' -l 'angelina.json'
# Author:      Dani Thomas
#
# Requires:    OpenCV, multiprocenv
# Based on: 
#-------------------------------------------------------------------------import argparse
from miaImages import DisplayImage
from miaWebImages import ReadImageWeb
from GoogleImages import GoogleImages
from miaFaceRecog import miaFaceRecog
from miaMultiProcEnv import MiaEnv

# construct the argument parser and parse the arguments

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--searchTerm", required=True,
	help="Google Search Term for finding faces")
ap.add_argument("-e", "--encodings", required=False,
	help="path to serialized db of facial encodings")
ap.add_argument("-l", "--labels", required=False,
	help="path to json file of face labels")
ap.add_argument("-w", "--waitKey", required=False,default=0,
	help="number of miliseconds to wait between pics 0 = unlimited, 100=one second")
 
args = vars(ap.parse_args())

googleImages = GoogleImages()
page = googleImages.Search(args["searchTerm"])
imageUrls = googleImages.GetImages(page,20,'jpg')
rdImage=ReadImageWeb(imageUrls,width=500)
disp=DisplayImage(waitKey=args["waitKey"])
faceRecog = miaFaceRecog(args["encodings"],args["labels"])
bsPrs=MiaEnv([rdImage, faceRecog, disp])


