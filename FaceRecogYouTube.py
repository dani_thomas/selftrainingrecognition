#! /usr/bin/env python
#--------------------------------------------------------------------------
# Copyright 2020 Cyber-Renegade.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Name:        FaceRecogYouTube.py
#              
# Purpose:     Detects faces from youtube video. In this case angelina jolie
#              and brad pitt. you can use the angelina.pickle and angelina.json
#              in faceData or create your own. just edit the json to label the
#              faces as required
#
# Author:      Dani Thomas
#
# Requires:    OpenCV, pafy, multiprocenv and ffpyplayer if you want sound
# Based on: 
#-------------------------------------------------------------------------
from miaCamera import DisplayCam
from miaFaceRecog import miaFaceRecog
from miaWebVideo import ReadWebVideo
from miaVideoFile import WriteVideofile
from miaMultiProcEnv import MiaEnv

displayVid = DisplayCam()
 

   
faceRecog = miaFaceRecog('angelina.pickle','angelina.json')

url = 'https://www.youtube.com/watch?v=IE-V4FeAufo'

readWebV = ReadWebVideo(url,'video','mp4','1280x544',cutdown=1)
writeVideo=WriteVideofile('./tempbongo.avi',width=1280,height=544)

bsPrs=MiaEnv([readWebV,faceRecog, writeVideo])
